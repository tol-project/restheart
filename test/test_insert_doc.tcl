package require json
package require TclCurl
package require http

set jsonBody {
{
  "variableType": "driver",
  "scenario":"historics",
  "model":"CCI_MainBrands_CBO_FBO_013",
  "name":"TempMax",
  "dating":"Monthly",
  "attributes":{
    "geography":"In",
    "simulable":"1"
  },
  "begin":{"$date":1104537600000},
  "end":{"$date":1448928000000},
  "values":[26.2, 29.2, 33, 35.8, 37.3, 36.5, 31.8, 32, 31.6, 31.7, 29.8, 27.1, 27.7, 32.1, 32.4, 36.1, 37.1, 35, 32.1, 31.4, 32.3, 32.7, 30, 27.8, 27.6, 29, 32.7, 36.8, 37.5, 35.2, 32.4, 31.6, 31.8, 32.3, 30.5, 27.3, 26.1, 27.7, 33, 35.5, 36.6, 33.3, 32, 30.9, 31.6, 32.6, 30.3, 28.5, 27.9, 30.8, 34, 37.1, 37.6, 36.8, 32.7, 32.7, 32.8, 32.5, 29.5, 27.6, 26.7, 29.8, 35, 38.1, 38.4, 36, 32.3, 31.8, 31.6, 32.3, 29.5, 26.7, 26.8, 29.4, 33.8, 35.4, 37.5, 34.9, 32.4, 31.5, 31.9, 33, 31, 27.9, 25.9, 29.3, 33.7, 36.1, 38.1, 37, 33.2, 31.8, 32.2, 32.7, 30.1, 28, 26.8, 29, 33.8, 36.3, 38.5, 34.3, 31.7, 31.4, 32.6, 31.7, 29.8, 27.5, 26.1, 28.3, 32.3, 36.6, 37.6, 37.5, 33.3, 32.9, 32.6, 33.1, 30.8, 27, 25.9, 30.1, 32.3, 35.2, 38.2, 35.4, 33.3, 32.7, 33.7, 34, 31.1, 28.4
}
}

set url http://52.50.99.11
set port 9080
set db db
set col BORRAME

if {0} {
set curlHandle [curl::init]

$curlHandle configure -url $url/${db}/$col \
    -port $port \
    -verbose 1 \
    -errorbuffer bufferError \
    -username a \
    -password a \
    -headervar responseHeader \
    -httpheader [list "Content-Type: application/json" ] \
    -postfields $jsonBody

set status [$curlHandle perform]
$curlHandle cleanup
}

puts "voy a buscar el documento"

set filter {{"attributes.geography": "In"}}
set keys {{"_id":1}}
set query [http::formatQuery filter $filter keys $keys]
puts "query is $query"

set curlHandle [curl::init]
$curlHandle configure -url $url/${db}/${col}?$query \
    -port $port \
    -verbose 0 \
    -errorbuffer bufferError \
    -username a \
    -password a \
    -headervar responseHeader \
    -bodyvar bufferBody

set status [$curlHandle perform]
parray responseHeader
puts "==========="
#puts $bufferBody
set dd [json::json2dict $bufferBody]
#puts $dd
foreach id [dict get $dd _embedded] {
    puts [dict get $id _id {$oid}]
}

$curlHandle cleanup
