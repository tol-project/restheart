package require snit
package require TclCurl
package require json
package require http
package require uri

snit::type ProgressText {
    variable startTime ""
    variable last 0
    variable lastTime 0
    
    option -total -default 100
    option -printcmd -default "puts -nonewline"
    option -printlncmd -default "puts"
    option -flushcmd -default "flush stdout"

    constructor {args} {
	$self configurelist $args
    }

    method start {} {
	set startTime [clock seconds]
	set last 0
	set lastTime 0
    }

    method advance { cur } {
	set now [clock seconds]
	set tot $options(-total)

	if {$cur > $tot} {
	    set cur $tot
	}
	if {($cur >= $tot && $last < $cur) ||
	    ($cur - $last) >= (0.05 * $tot) ||
	    ($now - $lastTime) >= 5} {
	    set last $cur
	    set lastTime $now
	    set percentage [expr round($cur*100/$tot)]
	    set ticks [expr $percentage/2]
	    if {$cur == 0} {
		set eta   ETA:[format %7s Unknown]
	    } elseif {$cur >= $tot} {
		set eta   TOT:[format %7d [expr int($now - $startTime)]]s
	    } else {
		set eta   ETA:[format %7d [expr int(($tot - $cur) * ($now - $startTime)/$cur)]]s
	    }
	    set lticks [expr 50 - $ticks]
	    set str "[format %3d $percentage]%|[string repeat = $ticks]"
	    append str "[string repeat . $lticks]|[format %8d [expr round($cur)]]|$eta\r"
	    {*}$options(-printcmd) $str
	    if {$cur >= $tot} {
		{*}$options(-printlncmd) ""
	    }
	    if {$options(-flushcmd) ne ""} {
		{*}$options(-flushcmd)
	    }
	}
    }
}

snit::type RESTHeart {
    variable responseHeader -array {}
    variable bufferError
    variable bufferBody
    variable URI -array {}
    
    variable progress
    delegate option -printcmd to progress
    delegate option -printlncmd to progress
    delegate option -flushcmd to progress

    typemethod toTolSet { txtList } {
	puts $txtList
	return "\[\[[join [lmap it $txtList {expr {[string is double $it]?$it:[string map [list %i $it] {"%i"}]}}] ,]\]\]"
    }

    typemethod tolWrite { msg } {
	if {[info exists ::TolConsole::widgets(output)]} {
	    upvar \#0 ::TolConsole::widgets(output) w_output
	    $w_output configure -state normal
	    $w_output replace {end-1char linestart} {end-1char lineend} $msg
	    $w_output configure -state disabled
	} else {
	    puts -nonewline $msg
	    #tol::console eval [string map [list "%m" $msg] {Write("%m")}]
	}
    }
    
    typemethod tolWriteLn { msg } {
	if {[info exists ::TolConsole::widgets(output)]} {
	    upvar \#0 ::TolConsole::widgets(output) w_output
	    $w_output configure -state normal
	    $w_output insert end "$msg\n"
	    $w_output configure -state disabled
	} else {
	    puts $msg
	    #tol::console eval [string map [list "%m" $msg] {WriteLn("%m")}]
	}
    }
    
    constructor { args } {
	install progress using ProgressText %AUTO%
	$self configurelist $args
	$self setURI "http://a:a@localhost:8080"
    }

    # this method is deprecated
    method setConnection { host port } {
	array set URI {
	    fragment {}
	    port {}
	    path {}
	    scheme http
	    host localhost
	    query {}
	    pwd a
	    user a
	}
	set URI(host) $host
	set URI(port) $port
	if {[info exists ::env(RESTHEART_AUTH)]} {
	    set auth [split $::env(RESTHEART_AUTH) ":"]
	    set URI(user) [lindex $auth 0]
	    set URI(pwd) [lindex $auth 1]
	}
    }

    method setURI {uri} {
	array set URI [uri::split $uri]
	if {$URI(port) eq {}} {
	    set URI(port) 8080
	}
    }
    
    method clearRequest { } {
	array unset responseHeader
	set bufferError ""
    }

    #dltotal dlnow ultotal ulnow
    method cbProgressUpload {dltotal dlnow ultotal ulnow} {
	if {$ultotal == 0} {
	    return
	}
	if {[$progress cget -total] == 0} {
	    $progress configure -total $ultotal
	    $progress start 
	}
	$progress advance $ulnow
    }
    
    method cbProgressDownload {dltotal dlnow ultotal ulnow} {
	#flush stdout
	#return
	if {$dltotal == 0} {
	    return
	}
	if {[$progress cget -total] == 0} {
	    $progress configure -total $dltotal
	    $progress start 
	}
	$progress advance $dlnow
    }

    # TODO: check that 201 is returned otherwise must be error
    method uploadFile { db path args } {
	$self clearRequest
	array set meta {
	    -verbose 0
	    -fs "fs.files"
	}
	array set meta $args
	set verbose $meta(-verbose)
	set fs $meta(-fs)
	array unset meta "-verbose"
	array unset meta "-fs"
	set filename [file tail $path]
	set listProps [list [string map [list "%f" $filename] {"filename":"%f"}]]
	foreach _k [array names meta] {
	    if {[string range $_k 0 0] eq "-"} {
		set k [string range $_k 1 end]
	    } else {
		set k $_k
	    }
	    if {$k ne ""} {
		lappend listProps "\"${k}\":\"$meta($_k)\""
	    }
	}
	set jsonProps "{[join $listProps ,]}"
	set curlHandle [curl::init]
	$progress configure -total 0
	
	$curlHandle configure -url $URI(scheme)://$URI(host)/${db}/$fs \
	    -port $URI(port) \
	    -verbose $verbose \
	    -noprogress 0 \
	    -progressproc [mymethod cbProgressUpload] \
	    -errorbuffer [myvar bufferError] \
	    -username $URI(user) \
	    -password $URI(pwd) \
	    -headervar [myvar responseHeader] \
	    -post 1 \
	    -httppost \
	      [list name file file $path contenttype application/octet-stream filename $filename] \
	    -httppost [list name "properties" contents $jsonProps]
	set status [$curlHandle perform]
	if {$status} {
	    set result "$status $bufferError"
	} else {
	    set location $responseHeader(Location)
	    
	    set result "0 [lindex [split $responseHeader(Location) /] end]"
	}
	$curlHandle cleanup
	return $result
    }

    method downloadFileActual { db id fileName args } {
	array set meta {
	    -verbose 0
	    -dest "."
	    -fs "fs.files"
	}
	array set meta $args
	set pathFile [file join $meta(-dest) $fileName]
	set curlHandle [curl::init]
	$progress configure -total 0
	
	$curlHandle configure -url $URI(scheme)://$URI(host)/${db}/$meta(-fs)/${id}/binary \
	    -port $URI(port) \
	    -verbose $meta(-verbose) \
	    -file $pathFile \
	    -noprogress 0 \
	    -progressproc [mymethod cbProgressDownload] \
	    -errorbuffer [myvar bufferError] \
	    -headervar [myvar responseHeader] \
	    -username $URI(user) \
	    -password $URI(pwd)
	{*}[$self cget -printlncmd] "Downloading file for id = $id to [file normalize $pathFile]"
	set status [$curlHandle perform]
	if {$status} {
	    set result "$status $bufferError"
	} else {
	    set httpCode [$curlHandle getinfo httpcode]
	    #puts "httpCode = $httpCode"
	    if {$httpCode == 200} {
		set result "0 [file normalize $pathFile]"
	    } else {
		set result "$httpCode [array get responseHeader]"
	    }
	}
	$curlHandle cleanup
	return $result
    }
    
    method downloadFile { project id args } {
	array set opts {
	    -nameasid 0
	    -ext ""
	}
	array set opts $args
	if {$opts(-nameasid)} {
	    set filename "${id}${opts(-ext)}"
	} else {
	    set finfo [$self getFileInfo $project $id {*}$args]
	    if {[lindex $info 0] != 0} {
		return "[lindex $info 0] [lindex $info 1]"
	    }
	    set filename [dict get [lindex $info 1] filename]
	}
	
	return [$self downloadFileActual $project $id $filename {*}$args]
    }
    
    method getFileInfo { db id args } {
	$self clearRequest
	array set meta {
	    -verbose 0
	    -fs "fs.files"
	}
	array set meta $args
	set curlHandle [curl::init]
	$progress configure -total 0
	
	$curlHandle configure -url $URI(scheme)://$URI(host)/${db}/${meta(-fs)}/${id} \
	    -port $URI(port) \
	    -verbose $meta(-verbose) \
	    -noprogress 0 \
	    -progressproc [mymethod cbProgressDownload] \
	    -errorbuffer [myvar bufferError] \
	    -username $URI(user) \
	    -password $URI(pwd) \
	    -bodyvar [myvar bufferBody]
	{*}[$self cget -printlncmd] "Downloading file metadata for id = $id"
	set status [$curlHandle perform]
	if {$status} {
	    set result "$status $bufferError"
	} else {
	    set httpCode [$curlHandle getinfo httpcode]
	    set getResult [json::json2dict $bufferBody]
	    if {$httpCode == 200} {
		set result [list 0 [dict get [json::json2dict $bufferBody] metadata]]
	    } else {
		set result [list $httpCode [dict get $getResult message]]
	    }
	}
	$curlHandle cleanup
	return $result
    }

    method deleteFile { db id args } {
	$self clearRequest
	array set meta {
	    -verbose 0
	    -fs "fs.files"
	}
	array set meta $args
	set curlHandle [curl::init]
	
	$curlHandle configure -url $URI(scheme)://$URI(host)/${db}/${meta(-fs)}/${id} \
	    -port $URI(port) \
	    -verbose $meta(-verbose) \
	    -customrequest "DELETE" \
	    -errorbuffer [myvar bufferError] \
	    -username $URI(user) \
	    -password $URI(pwd) \
	    -bodyvar [myvar bufferBody] \
	    -headervar [myvar responseHeader]
	set status [$curlHandle perform]
	if {$status} {
	    set result "$status $bufferError"
	} else {
	    set httpCode [$curlHandle getinfo httpcode]
	    switch -- $httpCode {
		204 {
		    set result "0 File Deleted"
		}
		404 {
		    set result "204 File Not Found"
		}
		default {
		    set result "$httpCode [array get responseHeader]"
		}
		
	    }
	}
	$curlHandle cleanup
	return $result
    }

    # curl -v -u a:a -X PUT 52.50.99.11:9080/cokindmvaind
    method createDB { db args } {
	$self clearRequest
	array set meta {
	    -verbose 0
	}
	array set meta $args
	set curlHandle [curl::init]
	
	$curlHandle configure -url $URI(scheme)://$URI(host)/$db \
	    -port $URI(port) \
	    -verbose $meta(-verbose) \
	    -customrequest "PUT" \
	    -errorbuffer [myvar bufferError] \
	    -username $URI(user) \
	    -password $URI(pwd) \
	    -bodyvar [myvar bufferBody] \
	    -headervar [myvar responseHeader]
	set status [$curlHandle perform]
	if {$status} {
	    set result "$status $bufferError"
	} else {
	    set httpCode [$curlHandle getinfo httpcode]
	    switch -- $httpCode {
		200 {
		    set result "0 Collection already exists"
		}
		201 {
		    set result "0 Collection created"
		}
		default {
		    set result "$httpCode [array get responseHeader] $bufferBody"
		}
		
	    }
	}
	$curlHandle cleanup
	return $result
    }

    method deleteDB { db args } {
	# find etag
	set etag [$self findDataBaseETag $db {*}$args]
	if {[lindex $etag 0]} {
	    return "[lindex $etag 0] [lindex $etag 1]"
	}
	$self deleteDataBaseActual $db [lindex $etag 1] {*}$args
    }

    method deleteDataBaseActual { db etag args } {
	$self clearRequest
	array set meta {
	    -verbose 0
	}
	array set meta $args
	set curlHandle [curl::init]
	
	$curlHandle configure -url "$URI(scheme)://$URI(host)/$db" \
	    -port $URI(port) \
	    -verbose $meta(-verbose) \
	    -customrequest "DELETE" \
	    -httpheader [list "If-Match: $etag"] \
	    -errorbuffer [myvar bufferError] \
	    -username $URI(user) \
	    -password $URI(pwd) \
	    -bodyvar [myvar bufferBody] \
	    -headervar [myvar responseHeader]
	set status [$curlHandle perform]
	if {$status} {
	    set result "$status $bufferError"
	} else {
	    set httpCode [$curlHandle getinfo httpcode]
	    switch -- $httpCode {
		204 {
		    set result "0 Collection Deleted"
		}
		404 {
		    set result "404 [dict get [json::json2dict $bufferBody] message]"
		}
		412 {
		    set result "412 Precondition Failed: may be ETag not found"
		}
		default {
		    set result "$httpCode [array get responseHeader]"
		}
		
	    }
	}
	$curlHandle cleanup
	return $result
	
    }
    
    # curl -v -u a:a -X PUT 52.50.99.11:9080/cokindmvaind/coll1
    method createCollection { db col args } {
	$self clearRequest
	array set meta {
	    -verbose 0
	}
	array set meta $args
	set curlHandle [curl::init]
	
	$curlHandle configure -url $URI(scheme)://$URI(host)/${db}/$col \
	    -port $URI(port) \
	    -verbose $meta(-verbose) \
	    -customrequest "PUT" \
	    -errorbuffer [myvar bufferError] \
	    -username $URI(user) \
	    -password $URI(pwd) \
	    -bodyvar [myvar bufferBody] \
	    -headervar [myvar responseHeader]
	set status [$curlHandle perform]
	if {$status} {
	    set result "$status $bufferError"
	} else {
	    set httpCode [$curlHandle getinfo httpcode]
	    switch -- $httpCode {
		200 {
		    set result "0 Collection already exists"
		}
		201 {
		    set result "0 Collection created"
		}
		default {
		    set result "$httpCode [array get responseHeader] $bufferBody"
		}
		
	    }
	}
	$curlHandle cleanup
	return $result
    }

    method createFileCollection { db col } {
	if {[string range $db end-[string length ".files"] end] eq ".files"} {
	    return [createCollection $db $col]
	} else {
	    return [list "406" "Bad collection name: it must ends in .files"]
	}
    }

    #curl -v -u a:a -H "Content-Type: application/json" 52.50.99.11:9080/cokindmvaind/QFS.files?pagesize=0
    #{"_embedded":[],"_id":"QFS.files","_etag":{"$oid":"58be871b14bafd31197efc3f"},"_returned":0}
    method findCollectionETag { db col args } {
	$self clearRequest
	array set meta {
	    -verbose 0
	}
	array set meta $args
	set curlHandle [curl::init]
	
	$curlHandle configure -url "$URI(scheme)://$URI(host)/${db}/${col}?pagesize=0" \
	    -port $URI(port) \
	    -verbose $meta(-verbose) \
	    -errorbuffer [myvar bufferError] \
	    -username $URI(user) \
	    -password $URI(pwd) \
	    -bodyvar [myvar bufferBody]
	set status [$curlHandle perform]
	if {$status} {
	    set result "$status $bufferError"
	} else {
	    set httpCode [$curlHandle getinfo httpcode]
	    set getResult [json::json2dict $bufferBody]
	    if {$httpCode == 200} {
		set result  [list 0 [dict get $getResult _etag {$oid}]]
	    } else {
		set result [list $httpCode [dict get $getResult message]]
	    }
	}
	$curlHandle cleanup
	return $result
    }
    
    method findDataBaseETag { db args } {
	$self clearRequest
	array set meta {
	    -verbose 0
	}
	array set meta $args
	set curlHandle [curl::init]
	
	$curlHandle configure -url "$URI(scheme)://$URI(host)/${db}?pagesize=0" \
	    -port $URI(port) \
	    -verbose $meta(-verbose) \
	    -errorbuffer [myvar bufferError] \
	    -username $URI(user) \
	    -password $URI(pwd) \
	    -bodyvar [myvar bufferBody]
	set status [$curlHandle perform]
	if {$status} {
	    set result "$status $bufferError"
	} else {
	    set httpCode [$curlHandle getinfo httpcode]
	    set getResult [json::json2dict $bufferBody]
	    if {$httpCode == 200} {
		set result  [list 0 [dict get $getResult _etag {$oid}]]
	    } else {
		set result [list $httpCode [dict get $getResult message]]
	    }
	}
	$curlHandle cleanup
	return $result
    }

    #curl -X DELETE -H 'If-Match: 58bfdf1214bafd14eadc8649' -H "Content-Type: application/json" 52.50.99.11:9080/cokindmvaind/Otra.files
    # < HTTP/1.1 204 No Content
    method deleteCollectionActual { db col etag args } {
	$self clearRequest
	array set meta {
	    -verbose 0
	}
	array set meta $args
	set curlHandle [curl::init]
	
	$curlHandle configure -url "$URI(scheme)://$URI(host)/${db}/$col" \
	    -port $URI(port) \
	    -verbose $meta(-verbose) \
	    -customrequest "DELETE" \
	    -httpheader [list "If-Match: $etag"] \
	    -errorbuffer [myvar bufferError] \
	    -username $URI(user) \
	    -password $URI(pwd) \
	    -bodyvar [myvar bufferBody] \
	    -headervar [myvar responseHeader]
	set status [$curlHandle perform]
	if {$status} {
	    set result "$status $bufferError"
	} else {
	    set httpCode [$curlHandle getinfo httpcode]
	    switch -- $httpCode {
		204 {
		    set result "0 Collection Deleted"
		}
		404 {
		    set result "404 [dict get [json::json2dict $bufferBody] message]"
		}
		412 {
		    set result "412 Precondition Failed: may be ETag not found"
		}
		default {
		    set result "$httpCode [array get responseHeader]"
		}
		
	    }
	}
	$curlHandle cleanup
	return $result
	
    }

    method deleteCollection { db col args } {
	# find etag
	set etag [$self findCollectionETag $db $col {*}$args]
	if {[lindex $etag 0]} {
	    return "[lindex $etag 0] [lindex $etag 1]"
	}
	$self deleteCollectionActual $db $col [lindex $etag 1] {*}$args
    }

    # https://softinstigate.atlassian.net/wiki/display/RH/Write+Requests
    method insertDocument { db col jsonDoc args } {
	$self clearRequest
	array set meta {
	    -verbose 0
	}
	array set meta $args
	set curlHandle [curl::init]
	
	$curlHandle configure -url $URI(scheme)://$URI(host)/${db}/${col} \
	    -port $URI(port) \
	    -verbose $meta(-verbose) \
	    -errorbuffer [myvar bufferError] \
	    -username $URI(user) \
	    -password $URI(pwd) \
	    -bodyvar [myvar bufferBody] \
	    -headervar [myvar responseHeader] \
	    -httpheader [list "Content-Type: application/json" ] \
	    -postfields $jsonDoc
	set status [$curlHandle perform]
	if {$status} {
	    set result "$status $bufferError"
	} else {
	    set httpCode [$curlHandle getinfo httpcode]
	    switch -- $httpCode {
		201 {
		    set result "0 [lindex [split $responseHeader(Location) /] end]"
		}
		406 {
		    set result "406 [dict get [json::json2dict $bufferBody] message]"
		}
		default {
		    set result "$httpCode [array get responseHeader] $bufferBody"
		}
		
	    }
	}
	$curlHandle cleanup
	return $result
    }

    method getOneDocument { db col oid args } {
	$self clearRequest
	array set meta {
	    -verbose 0
	}
	array set meta $args
	set keys {{"_id":0}}
	set curlHandle [curl::init]
	set query [http::formatQuery keys $keys]
	
	$curlHandle configure -url $URI(scheme)://$URI(host)/${db}/${col}/$oid?$query \
	    -port $URI(port) \
	    -verbose $meta(-verbose) \
	    -errorbuffer [myvar bufferError] \
	    -username $URI(user) \
	    -password $URI(pwd) \
	    -bodyvar [myvar bufferBody] \
	    -headervar [myvar responseHeader] \
	    -httpheader [list "Content-Type: application/json" ]
	set status [$curlHandle perform]
	if {$status} {
	    set result "$status $bufferError"
	} else {
	    set httpCode [$curlHandle getinfo httpcode]
	    switch -- $httpCode {
		200 {
		    # set getResult [json::json2dict $bufferBody]
		    # dict unset getResult _etag
		    # dict unset getResult _id
		    set result "0 $bufferBody"
		}
		404 {
		    set result "404 [dict get [json::json2dict $bufferBody] message]"
		}
		default {
		    set result "$httpCode [array get responseHeader]\n$bufferBody"
		}
		
	    }
	}
	$curlHandle cleanup
	return $result
    }

    method getDocuments { db col filter args } {
	$self clearRequest
	array set meta {
	    -verbose 0
	    -pagesize 100
	}
	#puts "filter = $filter"
	array set meta $args
	set page 0
	set curlHandle [curl::init]
	set allPages [list]
	set keys {{"_id":0, "_etag":0}}
	
	while 1 {
	    incr page
	    set query [http::formatQuery filter $filter page $page \
			   pagesize $meta(-pagesize) keys $keys]
	    $curlHandle configure -url $URI(scheme)://$URI(host)/${db}/${col}?$query \
		-port $URI(port) \
		-verbose $meta(-verbose) \
		-errorbuffer [myvar bufferError] \
		-username $URI(user) \
		-password $URI(pwd) \
		-headervar [myvar responseHeader] \
		-bodyvar [myvar bufferBody]
	    set status [$curlHandle perform]
	    if {$status} {
		$curlHandle cleanup
		return "$status $bufferError"
	    } else {
		set httpCode [$curlHandle getinfo httpcode]
		switch -- $httpCode {
		    200 {
			set getResult [json::json2dict $bufferBody]
			#puts $getResult
			#puts "returned = [dict get $getResult _returned]"
			lappend allPages $bufferBody
			if {[dict get $getResult _returned] < $meta(-pagesize)} {
			    # dict unset getResult _etag
			    # dict unset getResult _id
			    $curlHandle cleanup
			    set joined [join $allPages ","]
			    return [string map [list %R $joined] {0 [%R]}]
			    set result "0 [lmap id [dict get [json::json2dict $bufferBody] _embedded] {dict get $id _id {$oid}}]"
			}
		    }
		    default {
			$curlHandle cleanup
			return "$httpCode [array get responseHeader]\n$bufferBody"
		    }
		    
		}
	    }
	}
    }
    
    method findDocuments { db col filter args } {
	$self clearRequest
	array set meta {
	    -verbose 0
	}
	array set meta $args
	set keys {{"_id":1}}
	set query [http::formatQuery filter $filter keys $keys]
	#set query [http::formatQuery filter $filter]
	set curlHandle [curl::init]
	
	$curlHandle configure -url $URI(scheme)://$URI(host)/${db}/${col}?$query \
	    -port $URI(port) \
	    -verbose $meta(-verbose) \
	    -errorbuffer [myvar bufferError] \
	    -username $URI(user) \
	    -password $URI(pwd) \
	    -headervar [myvar responseHeader] \
	    -bodyvar [myvar bufferBody]
	set status [$curlHandle perform]
	if {$status} {
	    set result "$status $bufferError"
	} else {
	    set httpCode [$curlHandle getinfo httpcode]
	    switch -- $httpCode {
		200 {
		    # set getResult [json::json2dict $bufferBody]
		    # dict unset getResult _etag
		    # dict unset getResult _id
		    #puts "$bufferBody"
		    #puts [json::json2dict $bufferBody]
		    #puts [dict get [json::json2dict $bufferBody] _embedded]
		    set result "0 [lmap id [dict get [json::json2dict $bufferBody] _embedded] {dict get $id _id {$oid}}]"
		}
		default {
		    set result "$httpCode [array get responseHeader]\n$bufferBody"
		}
		
	    }
	}
	$curlHandle cleanup
	return $result	
    }

    method deleteDocument { db col id args } {
	$self clearRequest
	array set meta {
	    -verbose 0
	}
	array set meta $args
	set curlHandle [curl::init]
	
	$curlHandle configure -url $URI(scheme)://$URI(host)/${db}/${col}/${id} \
	    -port $URI(port) \
	    -verbose $meta(-verbose) \
	    -customrequest "DELETE" \
	    -errorbuffer [myvar bufferError] \
	    -username $URI(user) \
	    -password $URI(pwd) \
	    -bodyvar [myvar bufferBody] \
	    -headervar [myvar responseHeader]
	set status [$curlHandle perform]
	if {$status} {
	    set result "$status $bufferError"
	} else {
	    set httpCode [$curlHandle getinfo httpcode]
	    switch -- $httpCode {
		204 {
		    set result "0 Document Deleted"
		}
		404 {
		    set result "204 Document Not Found"
		}
		default {
		    set result "$httpCode [array get responseHeader] $bufferBody"
		}
		
	    }
	}
	$curlHandle cleanup
	return $result
    }

    method updateDocument { db col id args } {
    }
}

if {[info exists argv0] && [
    file dirname [file normalize [info script]/...]] eq [
    file dirname [file normalize $argv0/...]]} {

    set mongo [RESTHeart %AUTO%]
    $mongo setURI http://a:a@52.50.99.11:9080
    puts [$mongo deleteFile cokindmvaind 58bf24c814bafd14eadc85c5 -fs QFS.files -verbose 0]
}
